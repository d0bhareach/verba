mod home_page_widget;
mod model;
mod window;
use gdk::Display;
use gtk::prelude::*;
use gtk::{
    gdk,
    gio::{self, SimpleAction},
    glib::{self, clone},
    CssProvider,
};
use window::Window;

const APP_ID: &str = "com.gitlab.d0bhareach.verba";

// creare custom app in activate function call create new thread with spawn tread
// and loop for new clipboard event or for changes of content.
fn main() -> glib::ExitCode {
    // do not need this call because I use composite templates now
    // that what I thought but I need css file so need this resources register call
    gio::resources_register_include!("verba.gresource").expect("Failed to register resources.");

    // Create a new application
    let app = adw::Application::builder().application_id(APP_ID).build();

    // load css for UI
    app.connect_startup(|_| load_css());

    // Connect to "activate" signal of `app`
    app.connect_activate(build_ui);

    // Set keyboard accelerator to trigger "win.close".
    app.set_accels_for_action("win.close", &["<Ctrl>W"]);

    // Run the application
    app.run()
}
fn build_ui(app: &adw::Application) {
    // Create new window and present it
    // but not show it immediately, only in 5 seconsd
    // the only bad thing is that
    let window = Window::new(app);
    window.set_default_size(550, 800);
    // app.connect_activate(||)

    let focus_action = SimpleAction::new("close", None);
    focus_action.connect_activate(clone!(@weak window => move |_, _| {
        window.close();
    }));
    window.add_action(&focus_action);

    app.set_accels_for_action("close", &["<Ctrl>W"]);

    let display = gdk::Display::default().unwrap();
    let _clipboard = display.clipboard();
    window.present();
}

fn load_css() {
    // Load the CSS file and add it to the provider
    let provider = CssProvider::new();
    provider.load_from_resource("/com/gitlab/d0bhareach/verba/css/style.css");

    // Add the provider to the default screen
    gtk::style_context_add_provider_for_display(
        &Display::default().expect("Could not connect to a display."),
        &provider,
        gtk::STYLE_PROVIDER_PRIORITY_APPLICATION,
    );
}

#[cfg(test)]
mod tests {
    // use super::*;

    #[async_std::test]
    async fn my_test() -> std::io::Result<()> {
        async_std::task::spawn_local(async {
            let res = crate::model::get_hints("taufe").await;
            assert_eq!(res, vec!["taufen".to_string()]);
            Ok(())
        })
        .await
    }
}
