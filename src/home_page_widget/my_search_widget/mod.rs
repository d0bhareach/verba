mod my_search_imp;

use gtk::glib;

glib::wrapper! {
    pub struct MySearch(ObjectSubclass<my_search_imp::MySearchImp>)
        @extends gtk::Widget;
}
