#![allow(deprecated)]
use crate::model;
use gtk::prelude::EditableExt;
use gtk::prelude::EntryExt;
use gtk::Entry;
use gtk::{glib, subclass::prelude::*};

#[derive(Debug, Default, gtk::CompositeTemplate)]
#[template(file = "my_search.ui")]
pub struct MySearchImp {
    #[template_child]
    pub entry: TemplateChild<Entry>,
}

#[glib::object_subclass]
impl ObjectSubclass for MySearchImp {
    const NAME: &'static str = "MySearch";
    type Type = super::MySearch;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

// #[gtk::template_callbacks]
// impl MySearchImp {
//     #[template_callback]
//     fn handle_button_clicked(&self, _counter_button: &Button) {
//         let number_increased = self.counter.get() + 1;
//         self.counter.set(number_increased);
//         self.counter_label.set_label(&number_increased.to_string())
//     }
// }

impl ObjectImpl for MySearchImp {
    fn constructed(&self) {
        let completion_countries = gtk::EntryCompletion::new();
        completion_countries.set_text_column(0);
        completion_countries.set_minimum_key_length(1);
        completion_countries.set_popup_completion(true);

        let input_field = &self.entry;
        input_field.set_completion(Some(&completion_countries));
        input_field.connect_changed(|inp| {
            glib::spawn_future_local(glib::clone!(@weak inp => async move {
            let txt = inp.text().to_string();
            let txt = txt.trim();
            if txt.len().eq(&0) {
                return;
            }
            let words =  model::get_hints(txt).await;
            println!("{:?}", &words);
            let completion = inp.completion().expect("must have completion");
            let store = gtk::ListStore::new(&[glib::Type::STRING]);
            for w in words.iter() {
                store.set(&store.append(), &[(0, &w)]);
            }
            completion.set_model(Some(&store));
                }));
        });
    }
    fn dispose(&self) {
        self.dispose_template();
    }
}
impl BoxImpl for MySearchImp {}

impl WidgetImpl for MySearchImp {
    fn size_allocate(&self, width: i32, height: i32, baseline: i32) {
        self.parent_size_allocate(width, height, baseline);
    }
}
