use gtk::{glib, subclass::prelude::*};
use gtk::{Button, Label};
use std::cell::Cell;

#[derive(Debug, Default, gtk::CompositeTemplate)]
#[template(file = "counter.ui")]
pub struct CounterImp {
    #[template_child]
    pub counter_label: TemplateChild<Label>,
    pub counter: Cell<i32>,
}

#[glib::object_subclass]
impl ObjectSubclass for CounterImp {
    const NAME: &'static str = "Counter";
    type Type = super::Counter;
    type ParentType = gtk::Box;

    fn class_init(klass: &mut Self::Class) {
        klass.bind_template();
        klass.bind_template_callbacks();
    }

    fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
        obj.init_template();
    }
}

#[gtk::template_callbacks]
impl CounterImp {
    #[template_callback]
    fn handle_button_clicked(&self, _counter_button: &Button) {
        let number_increased = self.counter.get() + 1;
        self.counter.set(number_increased);
        self.counter_label.set_label(&number_increased.to_string())
    }
}

impl ObjectImpl for CounterImp {
    fn dispose(&self) {
        self.dispose_template();
    }
}
impl BoxImpl for CounterImp {}

impl WidgetImpl for CounterImp {
    fn size_allocate(&self, width: i32, height: i32, baseline: i32) {
        self.parent_size_allocate(width, height, baseline);
    }
}
