// widget to show counter
mod counter_imp;

use gtk::glib;

glib::wrapper! {
    pub struct Counter(ObjectSubclass<counter_imp::CounterImp>)
        @extends gtk::Widget;
}
