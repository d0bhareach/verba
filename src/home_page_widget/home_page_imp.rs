// home page implementation

use crate::home_page_widget::counter_widget::Counter;
use crate::home_page_widget::my_search_widget::MySearch;
use glib::subclass::InitializingObject;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, Box, CompositeTemplate, Widget};

#[derive(CompositeTemplate, Default)]
#[template(file = "home_page.ui")]
pub struct HomePageImp {
    #[template_child(id = "page-container")]
    pub container: TemplateChild<Box>,
    #[template_child]
    pub counter: TemplateChild<Counter>,
    #[template_child]
    pub search: TemplateChild<MySearch>,
}

#[glib::object_subclass]
impl ObjectSubclass for HomePageImp {
    // `NAME` needs to match `class` attribute of template
    const NAME: &'static str = "HomePage";
    type Type = super::HomePage;
    type ParentType = Widget;

    fn class_init(klass: &mut Self::Class) {
        Counter::ensure_type();
        MySearch::ensure_type();
        klass.bind_template();
    }
    fn instance_init(obj: &InitializingObject<Self>) {
        obj.init_template();
    }
}

impl ObjectImpl for HomePageImp {
    fn constructed(&self) {
        self.parent_constructed();
    }

    fn dispose(&self) {
        self.container.unparent();
        self.dispose_template();
    }
}

impl WidgetImpl for HomePageImp {}
