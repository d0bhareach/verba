// home page object

mod counter_widget;
mod home_page_imp;
mod my_search_widget;

use gtk::glib;

glib::wrapper! {
    pub struct HomePage(ObjectSubclass<home_page_imp::HomePageImp>)
        @extends gtk::Widget;
}
