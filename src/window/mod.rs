mod imp;

use gio::prelude::*;
use gio::ActionEntry;
use glib::Object;
use gtk::subclass::prelude::ObjectSubclassIsExt;
use gtk::{gio, glib};

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::ApplicationWindow, gtk::Window, gtk::Widget,
        @implements gio::ActionGroup, gio::ActionMap, gtk::Accessible, gtk::Buildable,
                    gtk::ConstraintTarget, gtk::Native, gtk::Root, gtk::ShortcutManager;
}

impl Window {
    pub fn new(app: &adw::Application) -> Self {
        // Create new window
        Object::builder().property("application", app).build()
    }

    pub fn setup_actions(&self) {
        let change_page_home = ActionEntry::builder("change-page-home")
            .activate(move |window: &Self, _, _| {
                window.imp().stack.set_visible_child_name("home");
            })
            .build();
        let change_page_a = ActionEntry::builder("change-page-a")
            .activate(move |window: &Self, _, _| {
                window.imp().stack.set_visible_child_name("page-a");
            })
            .build();
        let change_page_b = ActionEntry::builder("change-page-b")
            .activate(move |window: &Self, _, _| {
                window.imp().stack.set_visible_child_name("page-b");
            })
            .build();
        self.add_action_entries([change_page_home, change_page_a, change_page_b]);
    }
}
