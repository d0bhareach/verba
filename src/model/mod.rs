use once_cell::sync::OnceCell;
static WORDS: OnceCell<Vec<String>> = OnceCell::new();

pub async fn get_hints(start: &str) -> Vec<String> {
    let words = WORDS.get_or_init(|| {
        vec![
            "errichten".to_string(),
            "Krippe".to_string(),
            "versorgen".to_string(),
            "austreten".to_string(),
            "einziehen".to_string(),
            "Lohn".to_string(),
            "Besitz".to_string(),
            "Fürst".to_string(),
            "Verfassung".to_string(),
            "verklagen".to_string(),
            "Verwaltung".to_string(),
            "Gehalt".to_string(),
            "Pfarrer".to_string(),
            "Einnahme".to_string(),
            "abschaffen".to_string(),
            "taufen".to_string(),
            "Gebühr".to_string(),
            "beziehen".to_string(),
            "ereignen".to_string(),
            "Vorgang".to_string(),
        ]
    });
    words
        .iter()
        .filter(|w| {
            let w = w.to_lowercase();
            w.starts_with(start)
        })
        .map(|s| s.to_owned())
        .collect::<Vec<String>>()
}
