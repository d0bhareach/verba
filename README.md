# Verba
Verba - words in Latin language.    
This Application is created sprcifically for German language. And mimics some other Application that I have used many years ago to learn English language.     
## Behaviour
I would like to build my own set of words. So instead of writing them down with pen and paper I would like to save and learn words with this App. Plus I would like to be able to excercise words accodring to algorithms (write more about them later).   
So user suppose to use App as dictionary, but in order to avoid nasty proprietary problems user must fill it manually, from different sources. For some words I use up to 5 places where I get information about the word. I think that it's pity that German language is so poorly presented by these who claims to have learning material ready to master it, but it's the way it is. And this is one of the reasons why I decided to create this App.    
After user have added word to this Application it's searcheble and each time user has looked at this word the more frequently it would appear in exerceses. This way user will learn words qucker.

## Technical Stack
This App written with Rust Language and use Sqlite to save word. UI is created using gtk-rs crate which is Rust binding to GTK.